<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class DemoApiController extends FOSRestController
{
    public function getSomethingAction()
    {
        $data = array("something" => file_get_contents('http://loripsum.net/api'));
        $view = $this->view($data);
        return $this->handleView($view);
    }
}