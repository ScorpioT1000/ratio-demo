<?php
namespace AppBundle\Entity\OAuth;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @ORM\Entity
 */
class Client extends BaseClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
    }

    /** Workaround: client getter for self-auth
     * @param EntityManagerInterface $em
     * @return string public id
     * @throws \Exception */
    public static function retrieveFirstClient(EntityManagerInterface $em) {
        /** @var Client[] $c */
        $c = $em->getRepository('AppBundle\Entity\OAuth\Client')->findBy([], ['id' => 'ASC'], 1);
        if(empty($c)) { throw new \Exception('No clients found. Please refer README.md to create a client'); }
        return reset($c);
    }
}