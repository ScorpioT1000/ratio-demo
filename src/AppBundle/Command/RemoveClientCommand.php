<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/** Removes a client from the OAuth database */
class RemoveClientCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oauth:client:remove')
            ->setDescription('Removes a client by Public ID')
            ->addArgument('id', InputArgument::REQUIRED, 'Client Public ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        /** @var \FOS\OAuthServerBundle\Model\ClientManagerInterface $clientManager */
        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');
        $c = $clientManager->findClientByPublicId($id);
        if(! $c) {
            $output->writeln('Client not found');
        } else {
            $clientManager->deleteClient($c);
            $output->writeln('Successful!');
        }
    }
}