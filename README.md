ratio demo application
======================

Server Configuration
--------------------

1. Install __MySQL__

2. Install __nginx or apache__
    Detailed info: 
        Symfony Latest: http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html

3. Configure linux user: 
    http://symfony.com/doc/current/book/installation.html#checking-symfony-application-configuration-and-setup

4. Install composer to /usr/local/bin/composer:
	https://getcomposer.org/doc/00-intro.md#globally

Installation
------------

1. cd (document root directory)
2. git clone https://bitbucket.org/ScorpioT1000/ratio-demo.git
3. Setup database and other parameters:
    copy app/config/parameters.yml.dist as parameters.yml and configure it
4. composer update
5. Insert DB tables:
    php bin/console doctrine:schema:create
6. php bin/console cache:clear --env=dev --no-warmup
7. Register a new administrator: 
	> php bin/console fos:user:create admin email@example.com
	enter a new password
	> php bin/console fos:user:promote admin ROLE_ADMIN
8. Create a new OAuth client:
    > php bin/console oauth:client:create "http://example.com"
9. Go to example.com/app_dev.php (where example.com is your domain) and test the form
	
- By ScorpioT1000@yandex.ru, 2017