<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\OAuth\Client;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->get('doctrine.orm.entity_manager');
        $accessToken = null;
        $user = $this->getUser();
        if($user) {
            /** @var \FOS\OAuthServerBundle\Model\AccessTokenManagerInterface $accessTokenManager */
            $accessTokenManager = $this->get('fos_oauth_server.access_token_manager.default');
            $accessToken = $accessTokenManager->findTokenBy(['user' => $user]);
        }
        return $this->render('AppBundle:Default:index.html.twig', [
            'client' => Client::retrieveFirstClient($em),
            'user' => $user,
            'accessToken' => $accessToken ? $accessToken->getToken() : ''
        ]);
    }
}
