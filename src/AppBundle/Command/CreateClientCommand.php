<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/** Creates an OAuth client */
class CreateClientCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oauth:client:create')
            ->setDescription('Create client')
            ->addArgument('redirect_uri', InputArgument::REQUIRED, 'OAuth redirect URI')
            ->addOption('show-secret', null, InputOption::VALUE_OPTIONAL, 'Show client secret after creation?', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ru = $input->getArgument('redirect_uri');

        /** @var \FOS\OAuthServerBundle\Model\ClientManagerInterface $clientManager */
        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->createClient();
        $client->setRedirectUris(array($ru));
        $client->setAllowedGrantTypes(array('token', 'authorization_code', 'password'));
        $clientManager->updateClient($client);

        $output->writeln('Public ID: '.$client->getPublicId());
        if($input->hasOption('show-secret')) {
            $output->writeln('Secret: '.$client->getSecret());
        }
    }
}